# Openweathermap API test

### automated API test for a site Openweathermap on JS using mochawesome

## Check List:

- Verify that you can retrieve the current weather information for a specific location.
- Verify that you can retrieve a forecast for a specific location.
- Verify that you receive an error response when requesting weather information for an invalid location.
- Verify that the temperature in kelvins and celsius is the same

## how to run the test:

- clone the project to your computer
- install dependencies using the command `npm install`
- Add your own api key in variable `API_KEY_OPENWEATHERMAP` in .env file
- Type in the Command Prompt `npm test`

## test run example

![Photo](openweathermap_test_result_1.jpeg)

![Photo](openweathermap_test_result_2.jpeg)
