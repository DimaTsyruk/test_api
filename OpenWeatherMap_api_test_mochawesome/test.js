const axios = require('axios');
const chai = require('chai');
const expect = chai.expect;
const mocha = require('mocha');
const mochawesome = require('mochawesome');
require('dotenv').config();

describe('OpenWeatherMap API Tests', function () {
  const apiKey = process.env.API_KEY_OPENWEATHERMAP;
  const OPENWEATHERMAP_URL = 'https://api.openweathermap.org/data/2.5/weather';
  const locatoin = 'Kyiv';

  it('Should retrieve current weather information for Kyiv', async function () {
    const url = `${OPENWEATHERMAP_URL}?q=${locatoin}&appid=${apiKey}`;
    try {
      const response = await axios.get(url);
      expect(response.status).to.equal(200);
    } catch (error) {
      console.error('is not able to retrieve the current weather information for Kyiv');
      throw error;
    }
  });

  it('Should retrieve the coordinates for Kyiv', async function () {
    try {
      const url = `${OPENWEATHERMAP_URL}?q=${locatoin}&appid=${apiKey}`;
      const response = await axios.get(url);
      expect(response.data.coord.lon).to.equal(30.5167);
      expect(response.data.coord.lat).to.equal(50.4333);
    } catch (error) {
      console.error('is not able to retrieve the coordinates for Kyiv');
      throw error;
    }
  });

  it('Should catch an error when trying to to retrieve weather information for an invalid location.', async function () {
    const wrongLoction = 'Bikini Bottom';
    const url = `${OPENWEATHERMAP_URL}?q=${wrongLoction}&appid=${apiKey}`;
    try {
      const response = await axios.get(url);
    } catch (error) {
      expect(error.response.status).to.equal(404);
      expect(error.response.data.message).to.equal('city not found');
    }
  });

  it('Should catch an error when trying to to retrieve weather information for an invalid location.', async function () {
    const url = `${OPENWEATHERMAP_URL}?q=${locatoin}&appid=${apiKey}`;
    const url1 = `${OPENWEATHERMAP_URL}?q=${locatoin}&appid=${apiKey}&units=metric`;
    try {
      const response = await axios.get(url);
      const kelvinTemp = response.data.main.temp;
      const response1 = await axios.get(url1);
      const celsiusTemp = response1.data.main.temp;
      const differenceTemp = kelvinTemp - celsiusTemp;
      expect(differenceTemp).to.equal(273.15);
    } catch (error) {
      console.error('temperatures in Celsius and Kelvin do not match');
      throw error;
    }
  });
});
