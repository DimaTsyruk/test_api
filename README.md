# tests_API

### This repository contains API tests written in JavaScript using the mochawesome and allure report

Test cases, instructions for starting the project and examples of how tests work are in the README.md file inside the folder with the test
