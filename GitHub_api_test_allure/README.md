# GitHub API test

### automated API test for a site github on JS using allure

## Check List:

- Verify that a user is able to retrieve information about a specific repository using its name and owner.
- Verify that a user is able to create a new issue in a repository.
- Verify that a user is able to retrieve a list of pull requests associated with a repository.

## How to run the test:

- clone the project to your computer
- install dependencies using the command `npm install`
- Add your own api key in variable `GITHUB_API_KEY` in the .env file
- Type in the Command Prompt 'npm test' and 'npm run report'

## test run example

![Photo](github_test_result_1.jpg)

![Photo](github_test_result_2.jpg)
