const axios = require('axios');
const chai = require('chai');
const { expect } = require('chai');
const Mocha = require('mocha');
const mocha = new Mocha({
  reporter: 'mocha-allure-reporter',
});
require('dotenv').config();

const GITHUB_API_URL = 'https://api.github.com';
const GITHUB_API_KEY = process.env.GITHUB_API_KEY;
const owner = process.env.OWNER;
const repo = process.env.REPOSITORY_NAME;

describe('GitHub API Test', function () {
  it('Should retrieve repository information using owner name', async function () {
    try {
      const response = await axios.get(`${GITHUB_API_URL}/repos/${owner}/${repo}`, {
        headers: {
          Authorization: `Bearer ${GITHUB_API_KEY}`,
          Accept: 'application/vnd.github.v3+json',
        },
      });
      expect(response.status).to.equal(200);
      expect(response.data).to.deep.include({ name: repo });
      expect(response.data.owner).to.deep.include({ login: owner });
    } catch (error) {
      console.error('is not able to  retrieve repository information using owner name');
      throw error;
    }
  });

  it('Should create a new issue in a repository', async function () {
    const titleName = 'New issue';
    const bodyName = 'This is a new issue created by automated testing';
    try {
      const response = await axios.post(
        `${GITHUB_API_URL}/repos/${owner}/${repo}/issues`,
        {
          title: titleName,
          body: bodyName,
        },
        {
          headers: {
            Authorization: `Bearer ${GITHUB_API_KEY}`,
            Accept: 'application/vnd.github.v3+json',
          },
        }
      );
      expect(response.status).to.equal(201);
      expect(response.data).to.deep.include({ title: titleName, body: bodyName });
    } catch (error) {
      console.error('is not able to create a new issue in the repository');
      throw error;
    }
  });

  it('Should retrieve a list of pull requests associated with the repository', async function () {
    try {
      const response = await axios.get(`${GITHUB_API_URL}/repos/${owner}/${repo}/pulls`);
      expect(response.status).to.equal(200);
      expect(response.data).to.be.an('array');
    } catch (error) {
      console.error('is not able to retrieve a list of pull requests associated with the repository');
      throw error;
    }
  });

  it('Should catch an error when trying to to retrieve repository information using wrong owner name', async function () {
    try {
      const response = await axios.get(`${GITHUB_API_URL}/repos/WrongName/${repo}`, {
        headers: {
          Authorization: `Bearer ${GITHUB_API_KEY}`,
          Accept: 'application/vnd.github.v3+json',
        },
      });
    } catch (error) {
      expect(error.response.status).to.equal(404);
    }
  });

  it('Should catch an error when trying to to retrieve a list of pull requests associated with the non-existent repository', async function () {
    try {
      const response = await axios.get(`${GITHUB_API_URL}/repos/${owner}/UnrealRepository/pulls`);
    } catch (error) {
      expect(error.response.status).to.equal(404);
      expect(error.response.data).to.deep.include({ message: 'Not Found' });
    }
  });
});
